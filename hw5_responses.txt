question 1
    1. ((λp.(pz)) (λq.(w (λw.((((wq)z)p))))))
    2. (λp.((pq) (λp.(qp))))

question 2
    1.  λs.s z λq.s q
        s to λs, z is free, q to λq, s is free
    2.  (λs. s z) λq. w λw. w q z s
        s to λs, z is free, q to λq, first w is free, second w to λw, z is free, s is free
    3.  (λs.s) (λq.qs)
        s to λs, q to λq, s is free
    4.  λz. (((λs.sq) (λq.qz)) λz. (z z))
        s to λs, first q is free, second q to λq, first z to first λz, z(2 and 3) to second λz 

question 3
    1.  (λz.z) (λq.q q) (λs.s a)
        a a
    2.  (λz.z) (λz.z z) (λz.z q)
        q q
    3.  (λs.λq.s q q) (λa.a) b
        b b
    4.  (λs.λq.s q q) (λq.q) q
        q q
    5.  ((λs.s s) (λq.q)) (λq.q)
        (λq.q)

question 4
    1.  a or b = (a)(λxλy.x)(b)
        T or T = ((λxλy.x) (λxλy.x) (λxλy.x)) = (λxλy.x) = T
        F or F = ((λxλy.y) (λxλy.x) (λxλy.y)) = (λxλy.y) = F
        T or F = ((λxλy.x) (λxλy.x) (λxλy.y)) = (λxλy.x) = T
        F or T = ((λxλy.y) (λxλy.x) (λxλy.x)) = (λxλy.x) = T
    2.  The Church encoding for OR = (λp.λq. p p q)
        T or T = ((λp.λq. p p q) (λxλy.x) (λxλy.x)) = (λxλy.x)(λxλy.x)(λxλy.x) = T
        F or F = ((λp.λq. p p q) (λxλy.y) (λxλy.y)) = (λxλy.y)(λxλy.y)(λxλy.y) = F
        T or F = ((λp.λq. p p q) (λxλy.x) (λxλy.y)) = (λxλy.x)(λxλy.x)(λxλy.y) = T
        F or T = ((λp.λq. p p q) (λxλy.y) (λxλy.x)) = (λxλy.y)(λxλy.y)(λxλy.x) = T
question 5
    1.  Not p should turn p (a boolean) into the opposite of p
        p = T:  (λxλy.x)(λxλy.y)(λxλy.x) = (λxλy.y) = F
        p = F:  (λxλy.y)(λxλy.y)(λxλy.x) = (λxλy.x) = T
    2.  This is like in If-statement because if it is true then it execute the first expression and if it is false it runs the second expression.


        